#!/bin/zsh

# Set zsh as default shell
chsh -s $(which zsh)

# Create .zshrc file
touch ~/.zshrc


# Check if zplug is installed
if ! command -v zplug &> /dev/null
then
    # Install zplug if not installed
    curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh
fi


# Check if the system is macOS
if [[ "$OSTYPE" == "darwin"* ]]; then
  mv ./.zshrc_mac ~/.zshrc
  mv ./.p10k.zsh ~/.p10k.zsh
fi

# Check if the system is Ubuntu
if [[ "$(cat /etc/issue 2> /dev/null)" == *"Ubuntu"* ]]; then
  # Download Fira Code Nerd Font files
  wget -c https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.0/FiraCode.zip
  # Unzip font files to ~/.fonts directory
  unzip FiraCode.zip -d ~/.fonts
  # Install mkfontscale and mkfontdir using apt-get
  sudo apt-get update
  sudo apt-get install fontconfig
  # Create font directory if it doesn't exist
  mkdir -p ~/.local/share/fonts

  # Move font files to the font directory
  mv ~/.fonts/Fira*.ttf ~/.local/share/fonts/
  sudo fc-cache -fv
  # Add the repository to your sources list
  echo "deb [trusted=yes] https://apt.fury.io/rsteube/ /" | sudo tee /etc/apt/sources.list.d/fury.list

  # Update the package list
  sudo apt update

  # Install carapace-bin
  sudo apt install carapace-bin

  # Install atuin
  bash <(curl https://raw.githubusercontent.com/ellie/atuin/main/install.sh)

  mv ./.zshrc_ubuntu ~/.zshrc
  mv ./.p10k.zsh ~/.p10k.zsh
fi

# Check if the system is Debian
if [[ "$(cat /etc/issue 2> /dev/null)" == *"Debian"* ]]; then
  # Download Fira Code Nerd Font files
  wget -c https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.0/FiraCode.zip
  # Unzip font files to ~/.fonts directory
  unzip FiraCode.zip -d ~/.fonts
  # Install mkfontscale and mkfontdir using apt-get
  sudo apt-get update
  sudo apt-get install fontconfig
  # Create font directory if it doesn't exist
  mkdir -p ~/.local/share/fonts

  # Move font files to the font directory
  mv ~/.fonts/Fira*.ttf ~/.local/share/fonts/
  sudo fc-cache -fv
fi

# Check if the system is Manjaro
if [[ "$(cat /etc/issue 2> /dev/null)" == *"Manjaro"* ]]; then
  sudo pacman -Syu # Update system
  if ! command -v yay &> /dev/null # Check if yay is installed
  then
  sudo pacman -S --needed --noconfirm base-devel git # Install git and base-devel
  git clone https://aur.archlinux.org/yay-git.git # Clone yay repository
  sudo mv yay-git /opt/ # Move yay to /opt/ directory
  cd /opt/yay-git # Change directory to yay
  makepkg -si # Build and install yay
  fi
  pacman -S carapace-bin
  pacman -S atuin
  yay -S nerd-fonts-complete --noconfirm # Install Microsoft Fonts from AUR
fi



# Install and configure zoxide 
curl -sS https://webi.sh/zoxide | sh
source ~/.zshrc # Reload .zshrc file to apply changes
