#!/bin/bash

# 创建临时文件
temp_file=$(mktemp)

# 将要添加到.zshrc开头的内容写入临时文件
cat << 'EOF' > "$temp_file"
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
EOF

# 将原始.zshrc内容追加到临时文件
cat ~/.zshrc >> "$temp_file"

# 将要添加到.zshrc结尾的内容追加到临时文件
cat << 'EOF' >> "$temp_file"
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
eval \"\$(zoxide init zsh)\"
EOF

# 用临时文件覆盖.zshrc文件
mv "$temp_file" ~/.zshrc

# 删除临时文件（可选，因为已经使用mv命令覆盖了.zshrc）
rm -f "$temp_file"
